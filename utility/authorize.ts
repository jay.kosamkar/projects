//we create a function authorize and pass it as a middleware which works on excluded paths as well as included paths that needs authorization


import {IAuth} from "../app/routes/routes.types"
import {Request,Response,NextFunction} from "express";
import {verify} from "jsonwebtoken";

export const authorize=(excludedPaths:IAuth[])=>{
    return (req:Request,res:Response,next:NextFunction)=>{
        try{
            if (excludedPaths.find(p=>p.method===req.method && p.path===req.url)){
           //always remember to use return keyword
           return next()
            };
            const token =req.headers.authorization;
            const {SECRET_KEY}=process.env;
            if (token && SECRET_KEY){
                const payload= verify(token,SECRET_KEY);
                res.locals['user']=payload;
                //console.log(res.locals["user"])
                next();
            }else{
                res.send("token not found")
            }

        
        }catch(e){
            throw new Error("something went wrong");
            

        }
        

    }

}
//as permit is in array type the format  should be string[]
export const permit=(permittedRoles:string[])=>{
    return (req:Request,res:Response,next:NextFunction)=>{
        const user=res.locals["user"];
        const {role}=user;
        if(!permittedRoles.some(p=>role.includes(p))){
            return res.send("not authorized")
        }
        next()

    }
}