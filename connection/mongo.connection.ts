import {connect} from "mongoose";

export const connectToMongo=async()=>{
    try{  const {MONGO_CONNECTION}=process.env;
    //need to put if condition it traces the type also
    if(MONGO_CONNECTION){
    await connect(MONGO_CONNECTION)}
    console.log("database connected")

}catch(e){
    console.log("database not connected")

}
} 