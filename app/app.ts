//here connect to mongoDB
//create instance of express by assigning to app
//create a server starting function which is exported to index.ts file



import express from "express";
import {connectToMongo} from "../connection/mongo.connection"
import {registeredRoutes} from "./routes/routes.index"
export const startServer=async()=>{
    try{
       const app=express();
        await connectToMongo();
        registeredRoutes(app)

      

    const {PORT}=process.env;
    app.listen(PORT,
        ()=>{
            console.log(`SERVER STARTED ON ${PORT}`)
        })
    }catch(e){
        console.log("SERVER NOT STARTED")
    }
}


