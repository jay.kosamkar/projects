import { Application,Request,Response,NextFunction } from "express";
import {json} from "express";
import helmet from "helmet"
import { checkAdmin } from "../../utility/checkAdmin";
import {excludedPaths, routes} from "./routes.data";
import { authorize } from "../../utility/authorize";
import { responseHandler } from "../../utility/responseHandler";
export const registeredRoutes=async (app:Application)=>{
 try{
     app.use(json());
     app.use(helmet());
     //to check if a user or admin is present in a data base or not
     await checkAdmin();
     app.use(authorize(excludedPaths));

     for (let route of routes){
         app.use(route.path,route.router)

     };
     app.use((err:any,req:Request,res:Response,next:NextFunction)=>{
        res.status(err.statusCode  || 500).send(new responseHandler(null,err))
     })

 }catch(e){
     throw {message:"something went wrong"}

 }

}