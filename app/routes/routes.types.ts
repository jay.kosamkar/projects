import {Router} from "express"

export class Route{
    constructor(public path:string,public router:Router){}

};

export interface IAuth{
    method:"POST" | "GET" | "PUT" | "PATCH";
    path:string
}