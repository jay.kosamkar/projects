import {Route,IAuth} from "./routes.types";
import UserRouter from "../modules/users/users.routes";
import purchaseOrderRouter from "../modules/purchase_order/po.routes";
import FurnaceRouter from "../modules/furnace/furnace.routes";
import StorageRouter from "../modules/storage/storage.routes";
// import DeliveryRouter from "../modules/delivery/delivery.routes";
// import AccountsRouter from "../modules/accountant/accounts.routes";
import RoleRouter from "../modules/roles/roles.routes";
import ProductRouter from "../modules/product/products.routes";
import StatusRouter from "../modules/status/status.routes";
import CustomerRouter from "../modules/customer/customer.routes"




export const routes:Route[]=[

    //its a class so use new keyword 
    new Route("/user",UserRouter),
    new Route("/purchase-order",purchaseOrderRouter),
    new Route("/furnace",FurnaceRouter),
    new Route("/storage",StorageRouter),
    // new Route("/delivery",DeliveryRouter),
    // new Route("/payment",AccountsRouter),
    new Route("/roles",RoleRouter),
    new Route("/product",ProductRouter),
    new Route("/status",StatusRouter),
    new Route("/customer",CustomerRouter)
   
  


];

export const excludedPaths:IAuth[]=[
    //this is an interface
    {method:"POST",path:"/user/login"},
    {method:"POST",path:"/roles"},
    //{method:"GET",path:"/roles"}
]