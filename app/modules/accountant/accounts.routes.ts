import repoInfo from "./accounts.repo";

import { permit } from "../../../utility/authorize"
import { Router, Request, Response, NextFunction } from "express";
import { responseHandler } from "../../../utility/responseHandler";


const router=Router();



router.put("/update",permit(["62456dc514f78a5677794609"]),async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const {POId}=req.body;

    const result =await repoInfo.updateStatus(POId);
    res.send(new responseHandler(result,null))

    }catch(e){
        next(e)
    }
    

})

export default router