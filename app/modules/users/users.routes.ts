import {Router,Request,Response,NextFunction} from "express";
import ServiceInfo from "./users.service";
import {permit} from "../../../utility/authorize"
import * as shortID from "shortid";
import {generate} from "generate-password";
import { responseHandler } from "../../../utility/responseHandler";

const router=Router();


router.post("/",async(req:Request,res:Response,next:NextFunction)=>{
    try{
         // const id=shortID.generate();
    // const password=generate();

    const user=req.body;
    const result =await ServiceInfo.registerUser(user);

    res.send(new responseHandler(result,null))

    }catch(e){
        next(e)
    }
});
router.post("/login",async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const {username,password}=req.body;
        //console.log(req.body)
        const result=await ServiceInfo.userLogin(username,password);
        res.send(new responseHandler(result,null))

    }catch(e)
    {
        next(e)

    }
});
router.get('/', async(req:Request, res:Response, next:NextFunction) => {
    try {
      const result = await ServiceInfo.getAll()
      res.send(new responseHandler(result,null))
    } catch (e) {
      next(e)
    }
  });

router.put("/",async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const {id,...user}=req.body;
        const data=await ServiceInfo.userUpdate(id,user)
        res.send(new responseHandler(data,null))

    }catch(e){
        next(e)
    }
})
  

export default router