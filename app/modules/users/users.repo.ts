import { UserModel } from "./users.schema";
import { IUser } from "./users.types";

const registerUser = (User: IUser) => UserModel.create(User);

 const loginUser=(userId:string,password:string)=>UserModel.findOne({userId:userId,password:password})
    
const findUserById = (id: string) => UserModel.findOne({ _id: id });

const updateUser = (id: string, user:IUser) =>UserModel.updateOne({ _id: id }, { $push: { user } });

const deleteUser = (id: string) => UserModel.findByIdAndDelete(id);

const getAllUser = () => UserModel.find()


export default {registerUser,loginUser,updateUser,deleteUser,findUserById,getAllUser};