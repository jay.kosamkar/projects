import repoInfo from "./users.repo";
import {IUser} from "./users.types";
import * as shortID from "shortid";
import {generate} from "generate-password" 
import jwt from "jsonwebtoken";
const {sign}=jwt;



const registerUser=async(user:IUser)=>{
    try{ const id=shortID.generate();
        const password=generate();
        const data={...user,["id"]:id,["password"]:password}
        const result =await repoInfo.registerUser(data);
        console.log(data)
        return result

    }catch(e){
        console.log("something went wrong")
        
    }
   

}
// const userLogin=async(id:string,password:string)=>{
    const userLogin=async(userId:string,password:string)=>{
    try{
        const userRecord=await repoInfo.loginUser(userId,password);
        const {SECRET_KEY}=process.env;
        if(SECRET_KEY && userRecord){
            //use toObject() to convert userRecord into string
            const token=sign(userRecord.toObject(),SECRET_KEY);
            console.log(token)
            return token

        }

    }catch(e){
        console.log("something went wrong")

    }
}
const getAll = () => repoInfo.getAllUser();
const userUpdate=(id:string,user:IUser)=>repoInfo.updateUser(id,user)

export default{registerUser,userLogin,getAll,userUpdate}