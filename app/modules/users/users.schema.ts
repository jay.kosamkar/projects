import { Schema, model, Document } from "mongoose";
import { IUser } from "./users.types";

class UserSchema extends Schema {
  constructor() {
    super(
      {
        username: {
          type: String,
          required: true,
        },
        email: {
          type: String,
          required: true,
        },
        password: {
          type: String,
        },
        role: {
          type: Schema.Types.ObjectId,
          ref: "roles",
          required: true,
        },
      },
      {
        timestamps: true,
      }
    );
  }
}
type UserDocument = Document & IUser;
export const UserModel = model<UserDocument>("user", new UserSchema);