import { Schema, model, Document } from "mongoose";
import { IProducts } from "./products.types";

class ProductSchema extends Schema {
  constructor() {
    super({
    id:{
        type:Schema.Types.ObjectId
    },
      name: {
        type: String,
        required: true,
      },
      weight: {
        type: Number,
        required: true,
      },
      dimensions: {
        type: String,
        required: true,
      },
      quantity: {
        type: Number,
        required: true,
      },
      raw_materials_required: {
        name: {
            type:String,
            required: true,
        }
      },
      rate_per_unit: {
        type: Number,
        required: true,
      },
    },{
      timestamps: true,
    });
  }
}

type ProductDocument = Document & IProducts;
export const ProductModel = model<ProductDocument>("product", new ProductSchema());