import { responseHandler } from '../../../utility/responseHandler';
import { Router } from "express";
import serviceInfo from "./products.service";
import { permit } from '../../../utility/authorize';


const router = Router();

router.post('/',permit(["624878f68fe64afe4e062907"]),async(req,res,next)=> {
    try{
        const product = req.body
        const data = await serviceInfo.productCreate(product)
        res.send(new responseHandler(data,null))
    }catch(e){
        next(e)
    }
})

router.get('/',permit(["624878f68fe64afe4e062907"]),async(req,res,next)=> {
    try{
        const result = await serviceInfo.productsGet()
        res.send(new responseHandler(result,null))

    }catch(e){
        next(e)
    }
})

router.get('/:id',permit(["624878f68fe64afe4e062907"]),async(req,res,next)=> {
    try{
        const {id}= req.params
        console.log(id)
        const result = await serviceInfo.fetchById(id)
        res.send(new responseHandler(result,null))

    }catch(e){
        next(e)
    }
})

router.put('/:id',permit(["624878f68fe64afe4e062907"]),async(req,res,next)=> {
    try{
        const {id} = req.params;
        console.log(id)
        const updatedProduct = req.body
        const data = await serviceInfo.productUpdation(id, updatedProduct)
        res.send(new responseHandler(data,null))
        } catch(e){
        next(e)
    }
});


export default router