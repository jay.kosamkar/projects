import repoInfo from './products.repo';
import { IProducts } from './products.types';

const productCreate=(product:IProducts)=>repoInfo.createProduct(product);
const productsGet=()=>repoInfo.getProducts();
const fetchById=(id:string)=>repoInfo.getById(id);
const productUpdation=(_id:string,updatedProduct:IProducts)=>repoInfo.updateProduct(_id,updatedProduct)


export default  {productCreate,productsGet,fetchById,productUpdation}