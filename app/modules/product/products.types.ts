export interface IProducts {
    id:string
    name: string;
    weight: number;
    dimensions: string;
    quantity: number;
    raw_materials_required: string;
    rate_per_unit: number;
  }