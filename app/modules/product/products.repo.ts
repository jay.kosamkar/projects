//import { IProducts } from '../products/products.types';
import { ProductModel } from './products.schema';
import { IProducts } from './products.types';

const createProduct=(product:IProducts)=>ProductModel.create(product);
const getProducts=()=>ProductModel.find()
const getById=(id:string)=>ProductModel.findOne({_id:id});
const updateProduct=(_id:string,updatedProduct:IProducts)=>ProductModel.findByIdAndUpdate(_id,updatedProduct)


export default  {createProduct,getProducts,getById,updateProduct}