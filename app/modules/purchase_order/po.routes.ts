import { Router,Request,Response,NextFunction } from "express";
import { permit } from "../../../utility/authorize";
import { responseHandler } from "../../../utility/responseHandler";
import serviceInfo from "./po.service";

const router = Router();
//purchase-order/payment
router.put('/payment', async(req, res, next) => {
    try {

    } catch (e) {
        next(e)
    }
})





router.post('/',  permit(["62487adc8fe64afe4e062909"]), async (req: Request, res: Response, 
    next: NextFunction) => {
    try {
        const PO = req.body
        const result = await serviceInfo.POCreation(PO)
        res.send(new responseHandler(result,null))

    } catch (e) {
        next(e)
    }
})




router.get('/:id', permit(["62487adc8fe64afe4e062909","624878f68fe64afe4e062907"]), async (req: Request, res: Response, 
    next: NextFunction
) => {
    try {
        const {id} = req.params
        const result = await serviceInfo.fetchPOById(id)
        res.send(new responseHandler(result,null))
        
    } catch (e) {
        next(e)
    }
});

router.get('/', permit(["62487adc8fe64afe4e062909","624878f68fe64afe4e062907"]), async (req: Request, res: Response, 
    next: NextFunction
) => {
    try {
        const result = await serviceInfo.fetchPO();
        res.send(new responseHandler(result,null))

    } catch (e) {      
        next(e)
    }
})


export default router
