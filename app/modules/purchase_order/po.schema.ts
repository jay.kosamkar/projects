import { IPurchaseOrder } from "./po.types";

import { Schema, model, Document } from "mongoose";

class PurchaseOrderSchema extends Schema {
  constructor() {
    super(
      {
        customer_id: {
          type: Schema.Types.ObjectId,
          ref: "customers",
          required: true,
        },
        purchase_date: {
          type: String,
          required: true,
        },
        sales_person_id:{
            type: Schema.Types.ObjectId,
            ref: "users"
        },
        delivery_status: {
            type: Schema.Types.ObjectId,
            ref:"status",
            required: true,
        },
        products :[ {
          productId: {
            type: Schema.Types.ObjectId,
            ref: "products",
            required: true
          },
          status: {
              type: Schema.Types.ObjectId,
              default: "62492b951eab7629c59463e5",
              ref:"status",
              required: true,
          }
        }],
        delivery :{
            driver: {
                type: String,
            },
            vehicle_number: {
                type: String,
            },
            delivered_date: {
                type: String,
            }
        },
        payment :{
          
            
            total_amount :{
                type: Number,
            },
            payment_credit_days:{
                type: Number,
            },
            payment_date: {
                type: String,
            },
            payment_due_date:{
                type: String,
            },
            status:{
                type:Schema.Types.ObjectId
            }
        }
      },
      {
        timestamps: true,
      }
    );
  }
}

type PODocument = Document & IPurchaseOrder;
export const POModel = model<PODocument>("purchase Order", new PurchaseOrderSchema);