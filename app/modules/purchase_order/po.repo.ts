import { POModel } from "../purchase_order/po.schema";
import { IPurchaseOrder } from "./po.types";

const createPo = (PO: IPurchaseOrder) => POModel.create(PO);

const getPo = () => POModel.find();

const getPoById = (id: string) => POModel.findOne({ _id: id });




export default {createPo,getPo,getPoById}