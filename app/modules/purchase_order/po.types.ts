export interface IPurchaseOrder {
    customer_id: string;
    purchase_date: Date;
    delivery_status: string;
    sales_person_id: string;
    products: [
      {
        productId: string;
        status: string[];
      }
    ];
    delivery: {
      driver: string;
      vehicle_number: string;
      delivered_date: Date;
    };
    payment: {
      purchase_order_id: string;
      total_amount: number;
      payment_credit_days: number;
      payment_date: Date;
      payment_due_date: Date;
      status: string[];
    };
  }