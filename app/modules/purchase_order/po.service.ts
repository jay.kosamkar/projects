import repoInfo from "../purchase_order/po.repo";
import { IPurchaseOrder } from "./po.types";

const POCreation= (PO: IPurchaseOrder) => repoInfo.createPo(PO);

const fetchPO = () => repoInfo.getPo();

const fetchPOById = (id: string) => repoInfo.getPoById(id);




export default {POCreation,fetchPO,fetchPOById}