import serviceInfo from "./roles.service";
import {Router,Request,Response,NextFunction} from "express";
import { IRoles } from "./roles.types";
import { responseHandler } from "../../../utility/responseHandler";
import { permit } from "../../../utility/authorize";
 
const router=Router();

router.post("/",permit(["624878f68fe64afe4e062907"]),async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const role=req.body;
    const data=await serviceInfo.roleCreate(role)

    res.send(new responseHandler(data,null))

    }catch(e){
        next(e)
    }
})

router.get("/",async(req:Request,res:Response,next:NextFunction)=>{
try{
    const result = await serviceInfo.rolesGet();
    res.send(new responseHandler(result,null))
}catch(e){
    next(e)
}


})
router.get('/:id', async(req:Request,res:Response,next:NextFunction)=> {
    try{
        const {id} = req.params
        const result = await serviceInfo.getRolesById(id)
        res.send(new responseHandler(result,null))
        
    } catch(e){
        next(e)
    }
})

router.put('/:id', async(req:Request,res:Response,next:NextFunction) => {
    try{
        const {id} = req.params
        const userRole = req.body
        const result = await serviceInfo.roleUpdation(id, userRole)
        res.send(new responseHandler(result,null))
    } catch(e){
        next(e)
    }
})

router.delete('/:id',async(req:Request,res:Response,next:NextFunction)=> {
    try{
        const {id} = req.params
        const result=await serviceInfo.roleDeletion(id)
        res.send(new responseHandler(`${result} is delted`,null))
    } catch(e){
        next(e)
    }
})

export default router

