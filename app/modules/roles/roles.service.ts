import repoInfo from "./roles.repo";
import { IRoles } from "./roles.types";
const roleCreate=(userRole:IRoles)=>repoInfo.createRole(userRole)
const rolesGet=()=>repoInfo.getRoles();
const getRolesById = (id: string) => repoInfo.getRoleById(id);
const roleUpdation = (id: string, role:IRoles) =>repoInfo.updateRole(id,role)
const roleDeletion = (id: string) => repoInfo.deleteRole(id);
export default {roleCreate,rolesGet,getRolesById,roleUpdation,roleDeletion}