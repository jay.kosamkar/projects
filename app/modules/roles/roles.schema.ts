import {Schema,Document,model} from "mongoose";
import {IRoles} from "./roles.types"
class RolesSchema extends Schema{
    constructor(){
        super({ 

            role:{
                type:String,
                required:true
            },


        },{
            timestamps:true
        })
    }
}

type RolesDocument=Document & IRoles
export const RoleModel=model<RolesDocument>("roles",new RolesSchema)
