import {RoleModel} from "./roles.schema";
import { IRoles } from "./roles.types";

const createRole=(userRole:IRoles)=>RoleModel.create(userRole)
const getRoles=()=>RoleModel.find();
const getRoleById = (id: string) => RoleModel.findOne({ _id: id });
const updateRole = (id: string, role: IRoles) =>RoleModel.updateOne(
    { _id: id }, 
    { $push: { role } });
const deleteRole = (id: string) => RoleModel.findByIdAndDelete(id);
export default {createRole,getRoles,getRoleById,updateRole,deleteRole}