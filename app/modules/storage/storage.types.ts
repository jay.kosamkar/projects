export interface IStorage {
    name: string;
    purchase_order_inventory: [purchase_order_id: string, product_id: string];
    raw_materials_inventory: [name: string];
  }
  