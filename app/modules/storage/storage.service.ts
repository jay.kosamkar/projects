import repoInfo from "./storage.repo";
import { IStorage } from "./storage.types";


const racksCreation = (Storage: IStorage) => repoInfo.createRacks(Storage);
const racksFind=()=>repoInfo.findracks()
const fetchRacksById=(id:string)=>repoInfo.findRacksById(id)
const statusDelivery=(id:string)=>repoInfo.deliverystatus(id)

const racksUpdate= (id: string, data: any) =>repoInfo.updateracks(id,data);
    

      
const StorageDelete = (id: string) => repoInfo.deleteStorage(id);



export default {racksCreation,racksUpdate,StorageDelete,statusDelivery,racksFind,fetchRacksById};


