import { Schema, Types, model, Document } from "mongoose";
import { IStorage } from "./storage.types";

class StorageSchema extends Schema {
  constructor() {
    super(
      {
        name: {
          type: String,
          required: true,
        },
        purchase_order_inventory: [
          {
            purchase_order_id: {
              type: Types.ObjectId,
              ref: "purchase orders",
              required: true,
            },
            product_id: {
              type: Types.ObjectId,
              ref: "purchase orders",
              required: true,
            },
          },
        ],
        raw_materials_inventory: {
          type: String,
          required: true,
        },
      },
      {
        timestamps: true,
      }
    );
  }
}
type StorageDocument = Document & IStorage;
export const StorageModel = model<StorageDocument>("storage",new StorageSchema);