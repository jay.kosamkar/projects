import { body } from "express-validator";
import { validate } from "../../../utility/validate";

export const StorageValidator = [
  body("name").isString().notEmpty().withMessage("name is invalid"),
  body("purchase_order_id").isString().notEmpty().withMessage("purchase_order_id is invalid"),
  body("product_id").isString().notEmpty().withMessage("product_id is invalid"),
  validate,
];