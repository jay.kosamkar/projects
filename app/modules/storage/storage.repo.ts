import { POModel } from "../purchase_order/po.schema";
import { StorageModel } from "./storage.schema";
import { IStorage } from "./storage.types";

const createRacks = (Storage: IStorage) => StorageModel.create(Storage);
const findracks=()=>StorageModel.find();
const findRacksById=(id:string)=>StorageModel.findOne({_id:id})


//acces to delivery and 
const deliverystatus = (id: string) =>POModel.findOneAndUpdate({ _id: id },
    {
      $set: {
        $products: {
          status: "62487b458fe64afe4e06290f",
        },
      },
    }
  );

const updateracks = (id: string, data: any) =>POModel.updateOne({ _id: id }, 
    { $push:
         { delivery: data } 
        });

      
const deleteStorage = (id: string) => StorageModel.findByIdAndDelete(id);



export default {createRacks,updateracks,deliverystatus,findracks,deleteStorage,findRacksById};