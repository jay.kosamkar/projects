import { Router, Request, Response, NextFunction } from "express";
import { permit } from "../../../utility/authorize";
import { responseHandler } from "../../../utility/responseHandler";
import furnaceRepo from "../furnace/furnace.repo";
import serviceInfo from "./storage.service";
//import { } from "."

const router = Router();
router.post("/rack",permit(["62487ae88fe64afe4e06290b", "624878f68fe64afe4e062907"]),async(req: Request, res: Response, next: NextFunction) => {
    try {  
      const rack = req.body;
      const result = await serviceInfo.racksCreation(rack);
      res.send(new responseHandler(result,null));
    } catch (e) {
      next(e);
    }
  }
);
//storage/po/delivery
router.put("/po/delivery/:id",permit(["62487ae88fe64afe4e06290b", "624878f68fe64afe4e062907"]),async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const data = req.body;
      const result = await serviceInfo.racksUpdate(id, data);
      res.send(new responseHandler(result,null));
    } catch (e){
        next(e);

    }

    
    }
  
);
///storage/po/delivery/status
router.put("/po/delivery/status/:id",permit(["62487ae88fe64afe4e06290b", "624878f68fe64afe4e062907"]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const result = await serviceInfo.statusDelivery(id);
      res.send(new responseHandler(result,null));
    } catch (e) {
      next(e);
    }
  }
);

//storage/po/rack => assign rack to storage
router.put("/po/rack/:id",permit(["62487ae88fe64afe4e06290b", "624878f68fe64afe4e062907"]),async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const result = await furnaceRepo.updateStoredStatus(id);
      res.send(new responseHandler(result,null));
    } catch (e) {
      next(e);
    }
  }
);

// storage/rack => permit admin stores


// storage/rack => permit store and admin
router.get("/rack/:id",permit(["62487ae88fe64afe4e06290b", "624878f68fe64afe4e062907"]),async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const result = await serviceInfo.fetchRacksById(id);
      res.send(new responseHandler(result,null));
    } catch (e) {
      next(e);
    }
  }
);
//storage => permit storage and admin
router.get("/rack",permit(["62487ae88fe64afe4e06290b", "624878f68fe64afe4e062907"]),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const result = await serviceInfo.racksFind();
      res.send(new responseHandler(result,null));
    } catch (e) {
      next(e);
    }
  }
);

export default router;