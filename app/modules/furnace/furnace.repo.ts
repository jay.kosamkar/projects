import { POModel } from './../purchase_order/po.schema';
import { FurnaceModel } from "./furnace.schema";
import { IFurnace } from "./furnace.types";
import { IPurchaseOrder } from '../purchase_order/po.types';

const createFurnace = (Furnace: IFurnace) => FurnaceModel.create(Furnace);

const getFurnace = () => FurnaceModel.find();

const getFurnaceById = (id: string) => FurnaceModel.findOne({ _id:id});

const getPoIdFurnaceById = (purchase_order_id: string) => FurnaceModel.findOne({ purchase_order_id });




const updateProducingStatus = (id:string) => POModel.findOneAndUpdate({_id : id}, {
     $set : { 
        $products: {
        status: "62492ba21eab7629c59463e7"
  } 
}
});
const updateStoredStatus=(id:string)=>POModel.findOneAndUpdate({_id:id},{
    $set:{
        $products:{
            status:"62492bab1eab7629c59463e9"
        }
    }
})



export default {createFurnace,getFurnace,getFurnaceById,getPoIdFurnaceById,updateProducingStatus,updateStoredStatus}