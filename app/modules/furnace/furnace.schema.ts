import { Schema, model, Document } from "mongoose";
import { Types } from "mongoose";

import { IFurnace } from "./furnace.types";

class FurnaceSchema extends Schema {
  constructor() {
    super({
      name: {
        type: String,
        required: true,
      },
      purchase_order_id: {
        type:  Types.ObjectId ,
        ref: "purchase order",
        required: true,
      },
      product_id: {
        type:  Types.ObjectId ,
        ref: "purchase order",
        required: true,
      },
      labours: [{
        user_id : {
            type: Types.ObjectId,
            ref: "users",
            required: true,
        },
        shift : {
            type: String,
            required: true,
        }
      }],
    },{
      timestamps: true,
    });
  }
}

type FurnaceDocument = Document & IFurnace;
export const FurnaceModel = model<FurnaceDocument>("Furnace", new FurnaceSchema());