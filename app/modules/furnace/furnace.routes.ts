import { Router,Request,Response,NextFunction } from "express";
import { responseHandler } from "../../../utility/responseHandler";
import repoInfo from "../purchase_order/po.repo";
import ServiceInfo from "./furnace.service";
import { permit } from "../../../utility/authorize";
import { furnaceValidator } from "./furnace.validator";


const router=Router();

//admin and furnace men
router.post("/",permit(["624878f68fe64afe4e062907","62487b228fe64afe4e06290d"]),furnaceValidator,async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const furnace=req.body;
        const result =await ServiceInfo.furnaceCreation(furnace);
        res.send(new responseHandler(result,null))

    }catch(e){
        next(e)

    }
});
//fetching by id
router.get("/",permit(["624878f68fe64afe4e062907"]),async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const {id}=req.params;
        const result=await ServiceInfo.getFurnaceById(id);
        res.send(new responseHandler(result,null))

    }catch(e){
        next(e)
    }
});
//fetch all
router.get('/',permit(["624878f68fe64afe4e062907"]),async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const data =await ServiceInfo.getFurnace();
        res.send(new responseHandler(data,null))
    }catch(e){
        next(e)
    }
});
//updation by furnacemen as a completion and stored to storage
router.put("/po/status/:id",permit(["62487b228fe64afe4e06290d","624878f68fe64afe4e062907"]),furnaceValidator,async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const {id}=req.params
        const result =await ServiceInfo.storedStatusUpdate(id);
        res.send(new responseHandler(result,null))

    }catch(e){
        next(e)
    }
});

//producing status
router.put("/po/:id",permit(["62487b228fe64afe4e06290d","624878f68fe64afe4e062907"]),furnaceValidator,async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const {id}=req.params
        const result =await ServiceInfo.producingStatusUpdate(id);
        res.send(new responseHandler(result,null))

    }catch(e){
        next(e)
    }
})

export default router