import { POModel } from './../purchase_order/po.schema';
import repoInfo from "./furnace.repo"
import { IFurnace } from "./furnace.types";

const furnaceCreation = (furnace: IFurnace) => repoInfo.createFurnace(furnace);

const getFurnace = () => repoInfo.getFurnace();

const getFurnaceById = (id: string) => repoInfo.getFurnaceById(id);

const getPoIdFurnaceById = (purchase_order_id: string) => repoInfo.getPoIdFurnaceById(purchase_order_id);




const producingStatusUpdate = (id: string) =>repoInfo.updateProducingStatus(id);
const storedStatusUpdate=(id:string)=>repoInfo.updateStoredStatus(id)



export default { furnaceCreation,getFurnace,getFurnaceById,producingStatusUpdate,getPoIdFurnaceById,storedStatusUpdate}
 