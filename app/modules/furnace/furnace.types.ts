export interface IFurnace{
    name: string,
    purchase_order_id: string,
    product_id: string,
    labours: [
        user_id: string,
        shift: string,
    ],
    shift : string,
    _id: string,
}