import {IRacks} from "./racks.types"
import {Schema,model,Document } from "mongoose";


class RacksSchema extends Schema{
    constructor(){
        super({
            rackNumber:{
                type:Number,
                required:true
            },
            assignedToStorage:{
                type:Schema.Types.ObjectId,
                
            }




        })
    }
}

type RacksDocument=Document & IRacks;

export const RacksModel=model<RacksDocument>("storage racks",new RacksSchema)