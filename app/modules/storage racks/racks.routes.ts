import serviceInfo from "./racks.service";
import { permit } from "../../../utility/authorize"
import { Router, Request, Response, NextFunction } from "express";
import { responseHandler } from "../../../utility/responseHandler";

const router=Router();


router.post("/createRacks",permit(["62456d6514f78a5677794601"]),async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const racks=req.body;
        const result=await serviceInfo.rackCreate(racks);
        res.send(new responseHandler(result,null))


    }catch(e){
        next(e)
    }
   
})

export default router