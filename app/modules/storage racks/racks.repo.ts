import { RacksModel} from "./racks.schema";
import { IRacks } from "./racks.types";

const createRacks=(racks:IRacks)=>RacksModel.create(racks);

export default {createRacks}