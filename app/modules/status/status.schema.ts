import { IStatus } from './status.types';
import { Schema, model, Document } from "mongoose";

class StatusSchema extends Schema {
  constructor() {
    super({
      name: {
        type: String,
      },
    },{
      timestamps: true,
    });
  }
}

type StatusDocument = Document & IStatus;
export const StatusModel = model<StatusDocument>("status", new StatusSchema());