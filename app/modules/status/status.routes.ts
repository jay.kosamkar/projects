import { Router ,Request,Response,NextFunction} from "express";
import { responseHandler } from "../../../utility/responseHandler";
import serviceInfo from "./status.service";
import { permit } from "../../../utility/authorize";

const router=Router();

router.post("/",permit(["624878f68fe64afe4e062907"]),async(req:Request,res:Response,next:NextFunction)=>{
    
    try{
        const status=req.body;
        const data=await serviceInfo.statusCreation(status)
        res.send(new responseHandler(data,null))

    }catch(e){
        next(e)
    }



})
router.get('/', permit(["624878f68fe64afe4e062907"]),async(req:Request,res:Response,next:NextFunction)=> {
    try{
        const data = await serviceInfo.getStatus()
        res.send(new responseHandler(data,null))
        
    } catch(e){
        next(e)
    }
});

router.get("/:id",permit(["6245ee51a7c60ba6776d855d"]),async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;
        const data = await serviceInfo.StatusFindById(id);
        res.send(new responseHandler(data,null));
      } catch (e) {
        next(e);
      }
    }
  );
  
  router.put("/:id",permit(["6245ee51a7c60ba6776d855d"]),async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;
        const status = req.body;
        const updatedStatus = await serviceInfo.StatusUpdate(id, status);
        res.send(new responseHandler(updatedStatus,null));
      } catch (e) {
        next(e);
      }
    }
  );
export default router