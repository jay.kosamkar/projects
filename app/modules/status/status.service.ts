import repoInfo from "./status.repo";
import { IStatus } from "./status.types";

const statusCreation = (status: IStatus) => repoInfo.createStatus(status);
const getStatus = () => repoInfo.findStatus();
const StatusFindById = (id: string) => repoInfo.findStatusById(id);
const StatusUpdate = (id: string, status:IStatus) =>repoInfo.updateStatus(id,status);
const deleteStatus = (id: string) => repoInfo.deleteStatus(id);

export default {statusCreation,getStatus,StatusUpdate,deleteStatus,StatusFindById}