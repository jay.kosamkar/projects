import { StatusModel } from "./status.schema";
import { IStatus } from "./status.types";

const createStatus = (status: IStatus) => StatusModel.create(status);
const findStatus = () => StatusModel.find();
const findStatusById = (id: string) => StatusModel.findOne({ _id: id });
const updateStatus = (id: string, status: IStatus) =>StatusModel.updateOne({ _id: id }, { $push: { status } });
const deleteStatus = (id: string) => StatusModel.findByIdAndDelete(id);

export default {createStatus,findStatus,findStatusById,deleteStatus,updateStatus}