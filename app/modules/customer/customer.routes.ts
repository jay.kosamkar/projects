import { NextFunction, Router,Response,Request } from "express";
import { responseHandler } from "../../../utility/responseHandler";
import customerRepo from "./customer.service";
import {permit} from "../../../utility/authorize"


const router = Router();


router.post('/',permit(["624878f68fe64afe4e062907"]),async (req,res,next)=> {
    try{
        const customerData = req.body
        const result = await customerRepo.createCustomer(customerData)
        res.send(new responseHandler(result,null))

    } catch(e) {
        throw {e}
    }
})

router.get('/',permit(["624878f68fe64afe4e062907"]),async(req:Request,res:Response,next:NextFunction)=> {
    try{ const data = await customerRepo.getCustomers();
        res.send(new responseHandler(data,null))

    }catch(e){
        next(e)
    }
   
})

router.get('/',permit(["624878f68fe64afe4e062907"]),async(req:Request,res:Response,next:NextFunction)=> {
    try{
        const {id}= req.params
        const data = await customerRepo.findCustomerById(id)
        res.send(new responseHandler(data,null))

    }catch(e){
        next(e)
    }
  
})

router.put('/:id',permit(["624878f68fe64afe4e062907"]), async(req,res,next) => {
    try {
        const {id} = req.params
        const data1 = req.body;
        const data = await customerRepo.updateCustomer(id,data1);
        res.send(new responseHandler(data,null));
      } catch (e) {
        next(e);
      }
})

export default router