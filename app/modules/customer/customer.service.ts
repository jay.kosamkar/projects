import repoInfo from "./customer.repo";
import { ICustomer } from "./customer.types";

const createCustomer = (customer: ICustomer) => repoInfo.createCustomer(customer);
const getCustomers= () => repoInfo.getCustomers();
const findCustomerById = (id: string) => repoInfo.findCustomerById(id);
const updateCustomer = (id: string, customer:ICustomer) =>repoInfo.updateCustomer(id,customer);


export default {createCustomer,getCustomers,findCustomerById,updateCustomer}