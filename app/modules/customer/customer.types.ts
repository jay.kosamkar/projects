export interface ICustomer {
    name: string;
    contact: number;
    email: string;
  }