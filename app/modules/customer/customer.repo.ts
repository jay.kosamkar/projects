import { CustomerModel } from "./customer.schema";
import { ICustomer } from "./customer.types";

const createCustomer = (customer: ICustomer) => CustomerModel.create(customer);
const getCustomers= () => CustomerModel.find();
const findCustomerById = (id: string) => CustomerModel.findOne({ _id: id });
const updateCustomer = (id: string, updation: any) =>CustomerModel.updateOne({ _id: id }, { $push: { updation } });


export default {createCustomer,getCustomers,findCustomerById,updateCustomer}

