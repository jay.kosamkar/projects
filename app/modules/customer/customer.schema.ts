import { ICustomer } from './customer.types';
import { Schema, model, Document } from "mongoose";

class CustomerSchema extends Schema {
  constructor() {
    super({
      name: {
        type: String,
      },
      contact: {
        type: Number,
      },
      email: {
        type: String,
      },
    },{
      timestamps: true,
    });
  }
}

type CustomerDocument = Document & ICustomer;
export const CustomerModel = model<CustomerDocument>("customer", new CustomerSchema());